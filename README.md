# podman-test

## Getting started

locally test/dev node part
node 20 (`nvm use`)

```bash
corepack enable
pnpm i
pnpm start
curl "http://localhost:3000/"
# Hello World!
```

```bash
podman build . -t express-basic

podman run --rm -p 3000:3000 express-basic

curl "http://localhost:3000/"
# Hello World!
```

## License

MIT [License](./LICENSE)
